EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-misc-modules
LIBS:test-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x03 J2
U 1 1 613E3B7A
P 5850 4400
F 0 "J2" H 5850 4600 50  0000 C CNN
F 1 "POWER" V 6000 4400 50  0000 C CNN
F 2 "" H 5850 4400 50  0001 C CNN
F 3 "" H 5850 4400 50  0001 C CNN
	1    5850 4400
	0    -1   1    0   
$EndComp
Wire Wire Line
	5850 3900 5850 4000
Wire Wire Line
	5850 4000 5850 4200
Wire Wire Line
	5750 4200 5750 4100
Wire Wire Line
	5750 4100 5150 4100
Wire Wire Line
	5150 4100 5150 3000
Wire Wire Line
	5150 3000 5250 3000
Wire Wire Line
	6450 3000 6550 3000
Wire Wire Line
	6550 3000 6550 4100
Wire Wire Line
	6550 4100 5950 4100
Wire Wire Line
	5950 4100 5950 4200
$Comp
L Conn_01x03 J1
U 1 1 613E3CA6
P 4750 3550
F 0 "J1" H 4750 3750 50  0000 C CNN
F 1 "INPUT" V 4900 3550 50  0000 C CNN
F 2 "" H 4750 3550 50  0001 C CNN
F 3 "" H 4750 3550 50  0001 C CNN
	1    4750 3550
	-1   0    0    -1  
$EndComp
$Comp
L stereo-filter-module U1
U 1 1 613E3D57
P 5850 3250
F 0 "U1" H 5850 3250 60  0000 C CNN
F 1 "stereo-filter-module" H 5850 3350 60  0000 C CNN
F 2 "" H 5850 3250 60  0000 C CNN
F 3 "" H 5850 3250 60  0000 C CNN
	1    5850 3250
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J3
U 1 1 613E3E12
P 6950 3550
F 0 "J3" H 6950 3750 50  0000 C CNN
F 1 "OUTPUT" V 7100 3550 50  0000 C CNN
F 2 "" H 6950 3550 50  0001 C CNN
F 3 "" H 6950 3550 50  0001 C CNN
	1    6950 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3550 6650 3550
Wire Wire Line
	6650 3550 6650 4000
Wire Wire Line
	6650 4000 5850 4000
Wire Wire Line
	5850 4000 5050 4000
Connection ~ 5850 4000
Wire Wire Line
	5050 4000 5050 3550
Wire Wire Line
	5050 3550 4950 3550
Wire Wire Line
	4950 3450 5250 3450
Wire Wire Line
	5250 3550 5100 3550
Wire Wire Line
	5100 3550 5100 3650
Wire Wire Line
	5100 3650 4950 3650
Wire Wire Line
	6750 3450 6450 3450
Wire Wire Line
	6450 3550 6600 3550
Wire Wire Line
	6600 3550 6600 3650
Wire Wire Line
	6600 3650 6750 3650
$EndSCHEMATC
