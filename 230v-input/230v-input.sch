EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:230v-input-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x03 J1
U 1 1 613A282B
P 3500 3450
F 0 "J1" H 3500 3650 50  0000 C CNN
F 1 "230V" H 3500 3250 50  0000 C CNN
F 2 "myConnectors:JST-B3P-VH" H 3500 3450 50  0001 C CNN
F 3 "" H 3500 3450 50  0001 C CNN
	1    3500 3450
	-1   0    0    1   
$EndComp
$Comp
L Varistor RV1
U 1 1 613A28B8
P 4600 3700
F 0 "RV1" V 4725 3700 50  0000 C CNN
F 1 "SIOV-S10K275" V 4475 3700 50  0000 C CNN
F 2 "Varistors:RV_Disc_D12_W3.9_P7.5" V 4530 3700 50  0001 C CNN
F 3 "" H 4600 3700 50  0001 C CNN
	1    4600 3700
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR01
U 1 1 613A2A2A
P 3800 3650
F 0 "#PWR01" H 3800 3400 50  0001 C CNN
F 1 "Earth" H 3800 3500 50  0001 C CNN
F 2 "" H 3800 3650 50  0001 C CNN
F 3 "" H 3800 3650 50  0001 C CNN
	1    3800 3650
	1    0    0    -1  
$EndComp
$Comp
L Transformer_1P_2S T1
U 1 1 613A2A7C
P 5200 3700
F 0 "T1" H 5200 4200 50  0000 C CNN
F 1 "2x10.5V 16VA" H 5200 3200 50  0000 C CNN
F 2 "myTransformers:BREVE-TEZ-16-20" H 5200 3700 50  0001 C CNN
F 3 "" H 5200 3700 50  0001 C CNN
	1    5200 3700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J2
U 1 1 613A2B33
P 6100 3700
F 0 "J2" H 6100 3900 50  0000 C CNN
F 1 "2x12V" H 6100 3500 50  0000 C CNN
F 2 "myConnectors:JST-B3P-VH" H 6100 3700 50  0001 C CNN
F 3 "" H 6100 3700 50  0001 C CNN
	1    6100 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3350 4600 3550
Wire Wire Line
	4600 3850 4600 4050
Wire Wire Line
	3900 4050 4700 4050
Wire Wire Line
	3900 4050 3900 3550
Wire Wire Line
	3900 3550 3700 3550
Wire Wire Line
	3800 3650 3800 3450
Wire Wire Line
	3800 3450 3700 3450
Wire Wire Line
	4700 3350 4700 3500
Wire Wire Line
	4700 3500 4800 3500
Connection ~ 4600 3350
Wire Wire Line
	4700 4050 4700 3900
Wire Wire Line
	4700 3900 4800 3900
Connection ~ 4600 4050
Wire Wire Line
	5600 3300 5800 3300
Wire Wire Line
	5800 3300 5800 3600
Wire Wire Line
	5800 3600 5900 3600
Wire Wire Line
	5900 3800 5800 3800
Wire Wire Line
	5800 3800 5800 4100
Wire Wire Line
	5800 4100 5600 4100
Wire Wire Line
	5700 3800 5600 3800
Wire Wire Line
	5700 3600 5700 3800
Wire Wire Line
	5700 3600 5600 3600
Wire Wire Line
	5700 3700 5900 3700
Connection ~ 5700 3700
Wire Wire Line
	3700 3350 4700 3350
$EndSCHEMATC
